#version 300 es

// OUTPUT
out vec2 frag_pos;

void main()
{
    // Compute vertex position
    float x = -1.0 + float( ( gl_VertexID & 1 ) << 2 );
    float y = -1.0 + float( ( gl_VertexID & 2 ) << 1 );

    // Compute texture coordinates
    frag_pos.x = x * 0.5 + 0.5;
    frag_pos.y = y * 0.5 + 0.5;
    //texCoord = texCoord * 0.5 + 0.5;

    // Send position to clip space
    gl_Position = vec4( x, y, 0.0, 1.0 );
}
