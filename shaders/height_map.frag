#version 300 es
precision highp float;

// INPUT
in vec2 frag_pos;

// UNIFORMS
uniform float u_zoom;
uniform float u_mid_height;
uniform float u_ext_height;
uniform float u_power;
uniform float u_frequency;

// OUTPUT
layout(location=0) out vec4 frag_height;

float noise(in vec2 st)
{
    return fract(sin(dot(st.xy, vec2(42.9898000, 78.2330000))) * 43758.5453123);
}

vec3 permute(in vec3 x) {
    return mod(((x*34.0)+1.0)*x, 289.0);
}

/**
 * Simplex noise used to generate a value from a 2d vector
 */
float simplex(in vec2 v){
    const vec4 C = vec4(0.211324865405187, 0.366025403784439,
    -0.577350269189626, 0.024390243902439);
    vec2 i  = floor(v + dot(v, C.yy));
    vec2 x0 = v -   i + dot(i, C.xx);
    vec2 i1;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;
    i = mod(i, 289.0);
    vec3 p = permute(permute(i.y + vec3(0.0, i1.y, 1.0))
    + i.x + vec3(0.0, i1.x, 1.0));
    vec3 m = max(0.5 - vec3(dot(x0, x0), dot(x12.xy, x12.xy),
    dot(x12.zw, x12.zw)), 0.0);
    m = m*m;
    m = m*m;
    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;
    m *= 1.79284291400159 - 0.85373472095314 * (a0*a0 + h*h);
    vec3 g;
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 100.0 * dot(m, g);
}

void main() {

    float s = 0.0;
    float j;
    for (float i = 0.0; i < u_frequency; i += 1.0){
        j = pow(2.0, i);
        s += 1.0/j * simplex((frag_pos*u_zoom) * j);
    }

    float h = mix(u_mid_height, u_ext_height, pow(s, u_power));

    frag_height = vec4(h);
}


