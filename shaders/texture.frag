#version 300 es
precision highp float;

// INPUT
in vec2 frag_pos;

// UNIFORM
uniform sampler2D u_height_map;
uniform bool u_coloring;

// OUTPUT
layout(location=0) out vec4 out_bump;
layout(location=1) out vec4 out_solid_color;

/**
 * Compute the normalized normal of a given point in the heightmap
 */
vec3 compute_normal(in vec2 pos) {
    const vec2 size = vec2(1.0, 0.0);
    const ivec3 off = ivec3(-1, 0, 1);

    vec4 texel = texture(u_height_map, pos);
    float center = texel.x;
    float west = textureOffset(u_height_map, pos, off.xy).x;
    float east = textureOffset(u_height_map, pos, off.zy).x;
    float south = textureOffset(u_height_map, pos, off.yx).x;
    float north = textureOffset(u_height_map, pos, off.yz).x;
    vec3 w2e = normalize(vec3(size.xy, east - west));
    vec3 s2n = normalize(vec3(size.yx, north - south));

    vec3 normal = vec3(cross(w2e, s2n));

    return normal;
}

/**
 * Choose a color based on an altitude
 */
vec4 choose_color(in float h){
    if (h < 0.0)// UNDER THE SEA
    return vec4(1.0, 0.0, 0.0, 1.0);
    else if (h < 0.2)// SEA
    return vec4(0.0, 5.0*h/2.0, 10.0*h/2.0, 1.0);
    else if (h < 0.22)// BEACH
    return vec4(1.0, 1.0, 0.0, 1.0);
    else if (h < 0.45)// PLAIN
    return vec4(4.0*(0.55-h)/2.0, 8.0*(0.5-h)/2.0, 1.5*(0.55-h)/2.0, 1.0);
    else if (h < 0.7)// MOUNTAIN
    return vec4(0.2, 0.2, 0.075, 1.0);
    else if (h < 1.0)// ROCKS
    return vec4(0.5, 0.35, 0.0, 1.0);
    else
    // SNOW
    return vec4(1.0);
}

void main() {

    float height = texture(u_height_map, frag_pos).x;
    vec3 normal = compute_normal(frag_pos);

    out_bump = vec4(normal, height);

    if (u_coloring)
        out_solid_color = choose_color(height);
    else
        out_solid_color = vec4(1.0);
}
