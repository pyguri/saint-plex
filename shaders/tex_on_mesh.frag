#version 300 es
precision highp float;

#define PI 3.1415926535897932384626433832795

precision highp float;


// INPUT
in vec3 frag_position;
//in vec3 frag_normal;
in vec2 tex_position;

// UNIFORM
uniform vec3 u_light_color;
uniform vec3 u_light_position;
uniform float u_diffusion;
uniform float u_specular_reflect;
uniform float u_roughness;
uniform sampler2D u_solid_color;

uniform mat3 u_normal;
uniform sampler2D u_bump_map;

// OUPUT
out vec4 out_color;

/**
 * The Fresnel term of the BRDF, here we use the Schlick's approximation.
 *
 * Fresnel equations represent how much light a surface can transmit and reflect,
 * here we are only interested in the reflective factor.
 */
float schlickFresnel(in vec3 lDir, in vec3 hDir){
    return (u_specular_reflect + (1.0 - u_specular_reflect) * pow(1.0 - dot(lDir, hDir), 5.0));
}

/**
 * The distribution term of the BRDF, here we use the GGX (or Trowbridge-Reitz') approximation.
 *
 * This term represents how are distributed the facets that reflect light in a given direction.
 * The concentration of these facets is linked to the roughtness of the surface.
 */
float ggxDistribution(in vec3 normal, in vec3 hDir){
    return
    pow(u_roughness, 2.0)
    /
    (PI * pow(pow(dot(normal, hDir), 2.0) * (pow(u_roughness, 2.0) - 1.0) + 1.0, 2.0));
}

/**
 * The geometry term of the BRDF, here we use the Smith function with the Schlick's approximation.
 *
 * This term represent the proportion of facets blocked by other facets when reflecting light in a given direction.
 */
float geomSmith(in vec3 hDir, in vec3 lDir, in vec3 vDir){


    float k = pow(u_roughness + 1.0, 2.0) / 8.0;
    float gl = dot(hDir, lDir) / (dot(hDir, lDir) * (1.0 - k) + k);
    float gv = dot(hDir, vDir) / (dot(hDir, vDir) * (1.0 - k) + k);

    return (gl * gv);
}

/**
 * Bidirectional reflectance distribution function (BRDF) used in physically-based rendering shaders.
 * This function uses a microfacet model where the surface is seen as a set of
 *
 * This function allows us to compute the factor by witch the light must be multiplied in order to render a
 * realistic reflection on the current surface.
 */
float microfacetModel(in vec3 position, in vec3 normal){
    vec3 lightDir = normalize(u_light_position - position);
    vec3 viewDir = normalize(-position);
    vec3 halfDir = normalize(viewDir + lightDir);

    float diffuse = u_diffusion / PI;

    float specular = (
    schlickFresnel(lightDir, halfDir) *
    ggxDistribution(normal, halfDir) *
    geomSmith(halfDir, lightDir, viewDir))
    /
    (4.0 * dot(normal, lightDir) * dot(normal, viewDir));

    return (diffuse + specular);
}

void main() {
    vec3 lightDir = u_light_position - frag_position;
    vec3 light = u_light_color;

    vec4 bump = texture(u_bump_map, tex_position);
    vec3 normal = u_normal * bump.xyz;

    vec3 color = PI * light * microfacetModel(frag_position, normal) * dot(normal, lightDir);

    vec4 tex_color = texture(u_solid_color, tex_position);

    out_color = (vec4(color, 1.0) * tex_color);
}
