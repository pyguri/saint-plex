#version 300 es
precision highp float;


// INPUT
in vec2 frag_pos;

// UNIFORMS
uniform sampler2D u_height_map;
uniform sampler2D u_bump_map;
uniform sampler2D u_solid_color;
uniform int u_debug_mode;

// OUTPUT
out vec4 frag_color;

void main() {
    switch (u_debug_mode){
        default:
        case 0:
            frag_color = vec4(0.0);
            break;
        case 1:
            frag_color = texture(u_height_map, frag_pos);
            break;
        case 2:
            frag_color = texture(u_bump_map, frag_pos);
            break;
        case 3:
            frag_color = texture(u_solid_color, frag_pos);
            break;
    }
}
