#version 300 es

// INPUT
layout(location=1) in vec2 in_position;

// UNIFORM
uniform sampler2D u_bump_map;
uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;
//uniform mat3 u_normal;

// OUTPUT
out vec3 frag_position;
//out vec3 frag_normal;
out vec2 tex_position;

void main() {
    vec4 bump = texture(u_bump_map, in_position);
    float height = bump.a;

    vec3 pos = vec3(in_position.x, height, in_position.y);

    gl_Position = u_projection * u_view * u_model * vec4(pos, 1.0);

    frag_position = (u_view * u_model * vec4(pos, 1.0)).xyz;
//    frag_normal = normalize(u_normal * bump.xyz);
    tex_position = in_position;
}
