"use strict";

function init_wgl() {
    ewgl.continuous_update = true;

    init_ui();
    init_gle();
    load_shader_programs();
    load_texturing_environment(1024, 1024);
    build_texture_mesh(1024, 1024);

}

let passes = 0;

function draw_wgl() {

        if (check_generate.checked)
            generate_texture();
        else if (passes < 10){
            generate_texture();
            passes++;
        }

    clear_gle();
    draw_debug();
    update_uniforms();
    draw_meshes();
}

ewgl.launch_3d();