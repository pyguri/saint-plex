"use strict";

let slider_lr, slider_lg, slider_lb;

let slider_kd;
let slider_ks;
let slider_ns;
let slider_debug;
let slider_zoom;
let slider_pow;
let slider_ext_height;
let slider_mid_height;
let check_generate;
let check_color;
let slider_freq;

function init_ui() {
    UserInterface.begin();

    slider_debug = UserInterface.add_slider("Debug mode", 0, 3, 1, update_wgl());

    UserInterface.use_field_set('V', "Height map");
    UserInterface.use_field_set('H', null);
    check_generate = UserInterface.add_check_box("Continuous generation", false, update_wgl());
    check_color = UserInterface.add_check_box("Coloring", false, update_wgl());
    UserInterface.end_use();
    slider_zoom = UserInterface.add_slider("Zoom", 1, 100, 10, update_wgl());
    UserInterface.use_field_set('H', "Height");
    slider_mid_height = UserInterface.add_slider("From", 0, 100, 10, update_wgl());
    slider_ext_height = UserInterface.add_slider("To", 0, 100, 60, update_wgl());
    UserInterface.end_use();
    slider_pow = UserInterface.add_slider("Power", 0, 200, 100, update_wgl());
    slider_freq = UserInterface.add_slider("Frequency", 1, 100, 10, update_wgl());

    UserInterface.end_use();


    UserInterface.use_field_set('V', "Lighting");
    // Light color sliders
    UserInterface.use_field_set('H', "Color");
    slider_lr = UserInterface.add_slider(null, 0, 100, 100, update_wgl());
    set_widget_color(slider_lr, '#ff0000', '#ff0000');
    slider_lg = UserInterface.add_slider(null, 0, 100, 100, update_wgl());
    set_widget_color(slider_lg, '#00bb00', '#00ee00');
    slider_lb = UserInterface.add_slider(null, 0, 100, 100, update_wgl());
    set_widget_color(slider_lb, '#0000ff', '#0000ff');
    UserInterface.end_use();

    // Light variables
    UserInterface.use_field_set('H', "Model");
    slider_kd = UserInterface.add_slider('Diffusion', 0, 300, 100, update_wgl());
    slider_ks = UserInterface.add_slider('Scalar reflect', 0, 200, 50, update_wgl());
    slider_ns = UserInterface.add_slider('Roughness', 0, 200, 100, update_wgl());
    UserInterface.end_use();
    UserInterface.end_use();


    UserInterface.end();
}