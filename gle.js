"use strict";

let height_prg;
let tex_prg;
let height_map;
let bump_map;
let solid_color;
let height_fbo;
let bump_fbo;
let tex_width;
let tex_height;
let tex_vao;
let nb_tex_indices;
let debug_prg;
let display_prg;

/**
 * Initialize the environment (default values, etc.)
 */
function init_gle() {
    gl.clearColor(0, 0, 0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_DST_ALPHA);

    height_prg = null;
    tex_prg = null;
    height_map = null;
    bump_map = null;
    solid_color = null;
    height_fbo = null;
    bump_fbo = null;
    tex_width = 0;
    tex_height = 0;
    tex_vao = null;
    nb_tex_indices = 0;
    debug_prg = null;
    display_prg = null;
}

/**
 * Load shader program in the environment
 */
function load_shader_programs() {
    height_prg = ShaderProgramFromFiles("./shaders/canvas.vert", "./shaders/height_map.frag");
    tex_prg = ShaderProgramFromFiles("./shaders/canvas.vert", "./shaders/texture.frag");
    debug_prg = ShaderProgramFromFiles("./shaders/canvas.vert", "./shaders/debug.frag");
    display_prg = ShaderProgramFromFiles("./shaders/tex_on_mesh.vert", "./shaders/tex_on_mesh.frag");
}

/**
 * Create an OpenGL 2D texture, in RGBA
 * @param width width of the texture, in pixels
 * @param height height of the texture, in pixels
 * @returns {WebGLTexture}
 */
function new_texture(width, height) {
    let tex = gl.createTexture();

    gl.bindTexture(gl.TEXTURE_2D, tex);
    // Configure data type (storage on GPU) and upload image data to GPU
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, width, height, 0, gl.RGBA, gl.FLOAT, null);
    // Configure mode
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.bindTexture(gl.TEXTURE_2D, null);

    return tex;
}

/**
 * Generate a flat surface VAO, used to represent a texture in 3D
 * @param width number of vertices on the X axis
 * @param height number of vertices on the Y axis
 */
function build_texture_mesh(width, height) {

    let data_positions = new Float32Array(width * height * 2);
    for (let j = 0; j < height; j++) {
        for (let i = 0; i < width; i++) {
            data_positions[2 * (i + j * width)] = i / (width - 1);
            data_positions[2 * (i + j * width) + 1] = j / (height - 1);
        }
    }
    let vbo_positions = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_positions);
    gl.bufferData(gl.ARRAY_BUFFER, data_positions, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    let nbMeshQuads = (width - 1) * (height - 1);
    let nbMeshTriangles = 2 * nbMeshQuads;
    nb_tex_indices = 3 * nbMeshTriangles;
    let ebo_data = new Uint32Array(nb_tex_indices);
    for (let j = 0; j < height - 1; j++) {
        for (let i = 0; i < width - 1; i++) {
            // triangle 1
            ebo_data[6 * (i + j * width)] = i + j * width;
            ebo_data[6 * (i + j * width) + 1] = (i + 1) + j * width;
            ebo_data[6 * (i + j * width) + 2] = i + (j + 1) * width;
            // triangle 2
            ebo_data[6 * (i + j * width) + 3] = i + (j + 1) * width;
            ebo_data[6 * (i + j * width) + 4] = (i + 1) + j * width;
            ebo_data[6 * (i + j * width) + 5] = (i + 1) + (j + 1) * width;
        }
    }

    let ebo = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, ebo_data, gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    tex_vao = gl.createVertexArray();
    gl.bindVertexArray(tex_vao);
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_positions);
    gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(1);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);

    // Reset GL states
    gl.bindVertexArray(null);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
}

/**
 * Load the FBO in the environment
 * @param width width of the frame, in pixels
 * @param height height of the frame, in pixels
 */
function load_texturing_environment(width, height) {
    tex_width = width;
    tex_height = height;

    height_map = new_texture(tex_width, tex_height);
    bump_map = new_texture(tex_width, tex_height);
    solid_color = new_texture(tex_width, tex_height);

    height_fbo = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, height_fbo);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, height_map, 0);

    bump_fbo = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, bump_fbo);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, bump_map, 0);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT1, gl.TEXTURE_2D, solid_color, 0);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

/**
 * Generate a texture in the FBO with the two GLSL programs.
 * First we generate a heightmap in a internal texture.
 * We then use this texture to generate the bumpmap : a texture containing the direction of the normals and the height.
 * We also generate the solid color of the texture based on these information.
 */
function generate_texture() {
    // Using the height mapping FBO
    gl.bindFramebuffer(gl.FRAMEBUFFER, height_fbo);
    gl.viewport(0, 0, tex_width, tex_height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Generate height_map
    gl.useProgram(null);
    height_prg.bind();

    Uniforms.u_zoom = slider_zoom.value / 10;
    Uniforms.u_ext_height = slider_ext_height.value / 100;
    Uniforms.u_mid_height = slider_mid_height.value / 100;
    Uniforms.u_power = slider_pow.value / 100;
    Uniforms.u_frequency = slider_freq.value;

    gl.drawBuffers([gl.COLOR_ATTACHMENT0]);
    gl.drawArrays(gl.TRIANGLES, 0, 3);

    // Using the bump mapping FBO
    gl.bindFramebuffer(gl.FRAMEBUFFER, bump_fbo);
    gl.viewport(0, 0, tex_width, tex_height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Generate bump_map and color_map
    gl.useProgram(null);
    tex_prg.bind();

    Uniforms.u_coloring = check_color.checked;

    // Load previously generated height_map
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, height_map);
    Uniforms.u_height_map = 0;

    gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1]);
    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
}

function update_uniforms() {
    display_prg.bind();

    // Camera
    Uniforms.u_projection = ewgl.scene_camera.get_projection_matrix();
    Uniforms.u_view = ewgl.scene_camera.get_view_matrix();

    // Model matrix
    let model = Matrix.scale(0.6);
    model = Matrix.mult(model, Matrix.translate(-0.5, 0.0, -0.5));
    Uniforms.u_model = model;

    // Lighting & Shading
    Uniforms.u_normal = (Matrix.mult(ewgl.scene_camera.get_view_matrix(), model)).inverse3transpose();

    Uniforms.u_light_color = [slider_lr.value / 100, slider_lg.value / 100, slider_lb.value / 100];
    Uniforms.u_light_position = [0.0, 0.0, 0.0];

    Uniforms.u_diffusion = slider_kd.value / 100;
    Uniforms.u_specular_reflect = slider_ks.value / 100;
    Uniforms.u_roughness = slider_ns.value / 100;

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, bump_map);
    Uniforms.u_bump_map = 0;

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, solid_color);
    Uniforms.u_solid_color = 1;
}

function draw_meshes() {
    display_prg.bind();
    let drawMode = gl.TRIANGLES;
    gl.bindVertexArray(tex_vao);
    gl.drawElements(drawMode, nb_tex_indices, gl.UNSIGNED_INT, 0);
}

function draw_debug() {

    gl.disable(gl.BLEND);

    if (slider_debug.value > 0) {
        gl.useProgram(null);
        debug_prg.bind();

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, height_map);
        Uniforms.u_height_map = 0;

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, bump_map);
        Uniforms.u_bump_map = 1;

        gl.activeTexture(gl.TEXTURE2);
        gl.bindTexture(gl.TEXTURE_2D, solid_color);
        Uniforms.u_solid_color = 2;

        Uniforms.u_debug_mode = slider_debug.value;

        gl.viewport(0, 0, 256, 256);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    }

    gl.enable(gl.BLEND);
}

function clear_gle() {
    gl.bindVertexArray(null);
    gl.useProgram(null);
    gl.clear(gl.COLOR_BUFFER_BIT);
}